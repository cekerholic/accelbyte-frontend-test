import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css'
import './index.css'

import App from './App.vue'
import { store, key } from './store'

import router from '@/router/index'

createApp(App).use(router).use(store, key).use(ElementPlus).mount('#app')
