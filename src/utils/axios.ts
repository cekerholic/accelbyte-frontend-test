import Axios from 'axios'
import { ElMessage } from 'element-plus'
import { store } from '@/store'
import { MutationTypes } from '@/store/mutations'

const baseURL = 'https://jsonplaceholder.typicode.com'
const axios = Axios.create({
  baseURL,
  timeout: 30000
})

let refCount = 0
const setLoading = (loading: boolean) => {
  if (loading) {
    refCount++
    store.commit(MutationTypes.SET_LOADING, true)
  } else if (refCount > 0) {
    refCount--
    store.commit(MutationTypes.SET_LOADING, refCount > 0)
  }
}

axios.interceptors.request.use(
  (response) => {
    setLoading(true)
    return response
  },
  (error) => {
    setLoading(false)
    return Promise.reject(error)
  }
)

axios.interceptors.response.use(
  (response) => {
    setLoading(false)
    return response
  },
  (error) => {
    setLoading(false)
    if (error.response && error.response.data) {
      const code = error.response.status
      const msg = error.response.data.message
      ElMessage.error(`Code: ${code}, Message: ${msg}`)
      console.error(`[Axios Error]`, error.response)
    } else {
      ElMessage.error(`${error}`)
    }
    return Promise.reject(error)
  }
)

export default axios
