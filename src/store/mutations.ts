import { MutationTree } from 'vuex'
import { Photo, User, Album, PhotoCommented } from './types'
import { State } from './state'

// Mutations
export enum MutationTypes {
  SET_LOADING = 'SET_LOADING',
  SET_ALBUM_LIST = 'SET_ALBUM_LIST',
  SET_USER_LIST = 'SET_USER_LIST',
  SET_ACTIVE_ALBUM = 'SET_ACTIVE_ALBUM',
  SET_ACTIVE_ALBUM_PHOTOS = 'SET_ACTIVE_ALBUM_PHOTOS',
  SET_SELECTED_PHOTO = 'SET_SELECTED_PHOTO',
  SET_PHOTO_COMMENTED_LIST = 'SET_PHOTO_COMMENTED_LIST',
  SET_PHOTO_FAVORITED_LIST = 'SET_PHOTO_FAVORITED_LIST'
}

export type Mutations<S = State> = {
  [MutationTypes.SET_LOADING](state: S, payload: boolean): void
  [MutationTypes.SET_ALBUM_LIST](state: S, payload: Array<Album>): Array<Album>
  [MutationTypes.SET_USER_LIST](state: S, payload: Array<User>): Array<User>
  [MutationTypes.SET_ACTIVE_ALBUM](state: S, payload: Album): Album
  [MutationTypes.SET_ACTIVE_ALBUM_PHOTOS](
    state: S,
    payload: Array<Photo>
  ): Array<Photo>
  [MutationTypes.SET_SELECTED_PHOTO](state: S, payload: Photo): Photo
  [MutationTypes.SET_PHOTO_COMMENTED_LIST](
    state: S,
    payload: PhotoCommented
  ): Array<Photo>
  [MutationTypes.SET_PHOTO_FAVORITED_LIST](
    state: S,
    payload: Photo
  ): Array<Photo>
}

export const mutations: MutationTree<State> & Mutations = {
  [MutationTypes.SET_LOADING](state, payload: boolean) {
    state.loading = payload
  },
  [MutationTypes.SET_ALBUM_LIST](state, payload: Array<Album>) {
    state.albumList = payload
    return state.albumList
  },
  [MutationTypes.SET_USER_LIST](state, payload: Array<User>) {
    state.userList = payload
    return state.userList
  },
  [MutationTypes.SET_ACTIVE_ALBUM](state, payload: Album) {
    state.activeAlbum = payload
    return state.activeAlbum
  },
  [MutationTypes.SET_ACTIVE_ALBUM_PHOTOS](state, payload: Array<Photo>) {
    state.activeAlbumPhotos = payload
    return state.activeAlbumPhotos
  },
  [MutationTypes.SET_SELECTED_PHOTO](state, payload: PhotoCommented) {
    state.selectedPhoto = payload
    return state.selectedPhoto
  },
  [MutationTypes.SET_PHOTO_COMMENTED_LIST](state, payload: PhotoCommented) {
    state.photoCommentedList = state.photoCommentedList.concat(payload)
    return state.photoCommentedList
  },
  [MutationTypes.SET_PHOTO_FAVORITED_LIST](state, payload: Photo) {
    state.photoFavoritedList = state.photoFavoritedList.concat(payload)
    return state.photoFavoritedList
  }
}
