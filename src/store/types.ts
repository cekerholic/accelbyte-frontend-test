export interface Photo {
  albumId: number
  id: number
  title: string
  url: string
  favorited?: boolean
}

export interface PhotoCommented extends Photo {
  commented: boolean
  comment: string
}

export interface User {
  id: number
  name: string
  username: string
  email: string
  website: string
}

export interface Album {
  userId: number
  id: number
  title: string
}
