import { Photo, User, Album, PhotoCommented } from './types'

// State
export interface State {
  loading: boolean
  albumList: Array<Album>
  userList: Array<User>
  activeAlbum: Album
  activeAlbumPhotos: Array<Photo>
  selectedPhoto: PhotoCommented
  photoCommentedList: Array<PhotoCommented>
  photoFavoritedList: Array<Photo>
}
export const state: State = {
  loading: false,
  albumList: [],
  userList: [],
  activeAlbum: {} as Album,
  activeAlbumPhotos: [],
  selectedPhoto: {} as PhotoCommented,
  photoFavoritedList: [],
  photoCommentedList: []
}
