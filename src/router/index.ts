import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '@/views/Home.vue'
const AlbumDetails = () => import('@/views/AlbumDetails.vue')
const UserDetails = () => import('@/views/UserDetails.vue')
const PhotoDetails = () => import('@/views/PhotoDetails.vue')
const FavoritePhoto = () => import('@/views/FavoritePhoto.vue')

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/album/:album',
    name: 'AlbumDetails',
    component: AlbumDetails,
    children: [
      {
        path: 'photo/:id',
        name: 'PhotoDetails',
        component: PhotoDetails
      }
    ]
  },
  {
    path: '/user/:user',
    name: 'UserDetails',
    component: UserDetails
  },
  {
    path: '/favorites',
    name: 'FavoritePhoto',
    component: FavoritePhoto
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
