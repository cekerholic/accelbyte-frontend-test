# Photo Album Website

This web is developed with Vue 3 and Typescript in Vite.

## Build Setup

```bash
# install dependencies
$ yarn install

# run dev server
$ yarn dev

# build for production
$ yarn build 
```
